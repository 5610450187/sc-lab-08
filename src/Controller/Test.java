package Controller;

import java.util.ArrayList;

import Interface.Measurable;
import Interface.Taxable;
import Model.BankAccount;
import Model.Company;
import Model.Country;
import Model.Data;
import Model.Person;
import Model.PersonTex;
import Model.Product;
import Model.TaxCalculator;

public class Test {
	public static void main(String[] args){
		Test test = new Test();
		test.testPerson();
		test.testMin();
		test.testTax();
	}
	public void testPerson(){
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("meng",170);
		persons[1] = new Person("pim",160);
		persons[2] = new Person("afro",140);
		System.out.println(Data.average(persons));
		
	}
	public void testMin(){
		Measurable[] result = new Measurable[3];
		
		Measurable[] persons = new Measurable[3];
		persons[0] = new Person("meng",170);
		persons[1] = new Person("pim",160);
		result[0] = Data.min(persons[0], persons[1]);
		
		
		Measurable[] account = new Measurable[2];
		account[0] = new BankAccount("Uncle", 200);
		account[1] = new BankAccount("Jirayu", 1000);
		result[1] = Data.min(account[0], account[1]);
		
		Measurable[] country = new Measurable[2];
		country[0] = new Country("Thailand", 176220);
		country[1] = new Country("Belgium", 30510);
		result[2] = Data.min(country[0], country[1]);
		
		System.out.println(Data.average(result));
	}
	public void testTax(){	
		ArrayList<Taxable> t = new ArrayList<Taxable>();
		t.add(new PersonTex("meng", 17000));
		t.add(new PersonTex("pim", 450000));
		t.add(new Company("KFC", 150000));
		t.add(new Company("MK", 600000));
		t.add(new Product("Shirt",300));
		t.add(new Product("Watch", 2500));
		
		System.out.println(TaxCalculator.sum(t));
		
	}
}
