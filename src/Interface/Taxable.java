package Interface;

public interface Taxable {
	public double getTax(double price);
}
