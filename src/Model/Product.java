package Model;
import Interface.Taxable;

public class Product implements Taxable{
	private double total;
	private String name;
	private double money;
	public Product(String name,double money) {
		this.name = name;
		this.money = money;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	@Override
	public double getTax(double price) {
		total = ( price * 7 ) / 100;
		return total;
	}

}
