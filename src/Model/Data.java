package Model;

import Interface.Measurable;

public class Data {
	public static double average(Measurable objects[]){
		double sum = 0;
		for (Measurable measurables : objects) {
			sum+= measurables.getMeasure();
		}
		if(objects.length>0){
			return sum/objects.length;
		}
		return 0;

	}
	public static Measurable min(Measurable m1,Measurable m2){
		if(m1.getMeasure()> m2.getMeasure()){
			return m2;
		}else{
			return m1;
		}
	}
	
}
