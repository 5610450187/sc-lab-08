package Model;

import Interface.Measurable;

public class BankAccount implements Measurable {
	String name;
	double money;
	
	public BankAccount(String name,double money) {
		this.name = name;
		this.money = money;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	@Override
	public double getMeasure() {
		return money;
	}
	
}
