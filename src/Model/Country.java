package Model;

import Interface.Measurable;

public class Country implements Measurable {
	String name;
	double area;
	public Country(String name,double area) {
	this.name = name;
	this.area = area;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		this.area = area;
	}
	@Override
	public double getMeasure() {
		return area;
	}
	
 
}
