package Model;
import Interface.Taxable;

public class Company implements Taxable{
	private double total;
	private String name;
	private double money;
	public Company(String name,double money) {
	this.name = name;
	this.money = money;
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	@Override
	public double getTax(double price) {
		total = ( price * 30 )/ 100 ;
		return total;
	}

}
