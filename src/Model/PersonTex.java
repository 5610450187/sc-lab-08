package Model;

import Interface.Taxable;

public class PersonTex implements Taxable{
	double total;
	String name;
	double money;
	public PersonTex(String name,double money) {
		this.name = name;
		this.money = money;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	@Override
	public double getTax(double price) {
		if(price <= 300000){
			total = (price *5)/100;
		}else{
			total = total + ((price *10)/100);
		}
		return total;
	}

}
