package Model;

import java.util.ArrayList;

import Interface.Taxable;

public class TaxCalculator {
	public static double sum (ArrayList<Taxable> taxList) {
		double total= 0 ;
		for (int i = 0; i < taxList.size(); i++) {
			total += taxList.get(i).getTax(i);
			
		}return total;
	}
}
